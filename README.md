# Hvad er dette?
Dansker termer for alt inden for datalogiens verden.

# Termer:
```
computer: datamat
laptop: mappedatamat
```

## Systemer og sprog
```
kernel: kerne
shell: skald
trap: fælde
exception: undtagelse

source code: kildekode
interpreter: fortolker
compiler: oversætter
transpiler: tværsoversætter
linker: sammenkæder
namespace: navnerum
identifier: identifikator
loop: løkke
expression: udtryk
statement: erklæring (nogle gange også udtryk)
if (statement/expression): hvis(udtryk/erklæring)
while loop: (i)mens løkke
for loop: forløkke
```

## Objekt-orienteret
```
object: objekt
class: klasse
inheritence: nedarvning
interface: grænseflade
superclass: forfædre/forældreklasse/overklasse
subclass: arvningklasse/børneklasse/underklasse
instance: instans/forekomst
method: metode
attribute: attribut
property: egenskab
field: felt
```

## Hårdevarer
```
CPU: (Central)beregningsenhed
core: kerne
memory/RAM: hukommelse
cache: mellemlager
storage: lager
SSD: solidtilstandslager
HDD: fastpladelager
```

## Git
```
git: nar (sjælden brug)
pull: træk/hiv/hal
push: skub
commit: forpligt/forpligtelse
branch: gren/afgren
master: mester
merge: flet/sammenflæt
cherry-pick: håndplukke
blame: bebrejde/beskyld
pull-request: trækkeanmodning
merge-request: sammenfletningsanmodning
stash: gem
issue: problem
checkout: tjek ud
repository: depot
```

## Teknologier
```
blockchain: e-guirlande
bitcoin: e-mønt
```
## Internet
```
WWW: verdensomspændende spindel
URL: ensartet resurselokator
DNS: domænenavnsservice
TCP: overførselskontrolsprotokol
E-mail: e-brev
downloading: nedhenting
loading: indlæse
firewall: brandvæg/ildmur/cybervoldgrav
phishing: phiske/feske
```

## Telefon
```
smartphone: lommedatamat
android: androide
```
